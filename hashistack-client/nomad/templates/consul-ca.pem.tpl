{{ with secret "pki_int_hamsterwheel/issue/consul-hashistack-tls" "common_name=client.raspi-local.hashistack.hamsterwheel.xyz" }}
{{ .Data.issuing_ca }}
{{ end }}
