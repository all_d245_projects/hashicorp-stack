job "pi-hole" {
  datacenters = ["raspi-local"]
  #datacenters = ["homelab"]
  type = "system"
  
  constraint {    
    attribute = "${attr.kernel.name}"
    value     = "linux"  
  }

  #constraint {    
  #  attribute = "${node.class}"    
  #  value     = "raspberry-pi"  
  #}

  group "pi-hole" {
    network {
      mode = "bridge"
      port "dhcp" {
	static       = 67
        to           = 67
        host_network = "tailscale-private"
      }
      port "dns" {
        static       = 53
        to           = 53
        host_network = "tailscale-private"
      }
      port "http" {
        static       = 8080
        to           = 80
        host_network = "tailscale-private"
      }
    }

    service {
      name = "pi-hole"
      
      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=false",
        "traefik.http.routers.pi-hole.entrypoints=https",
        "traefik.http.routers.pi-hole.rule=Host(`pi-hole.hamsterwheel.xyz`)",
        "traefik.http.routers.pi-hole.tls=true",
        "traefik.http.routers.pi-hole.tls.certresolver=le-prod",
        "traefik.http.services.pi-hole.loadbalancer.server.port=8080",
        "traefik.http.routers.pi-hole.middlewares=pi-hole",

        # registered middlewares
        "traefik.http.middlewares.add-admin-prefix.addprefix.prefix=/admin",
        "traefik.http.middlewares.pi-hole.chain.middlewares=redirect-to-https@file,add-admin-prefix"
      ]
 
      port = "http"     
        check {
        name     = "pi-hole healthcheck"
        type     = "http"
        path     = "/"
        interval = "10s"
        timeout  = "2s"
      }
    }
    task "server" {
      driver = "docker"
      config {        
        image = "pihole/pihole:latest"
        ports = [
          "dns",
          "dhcp",
          "http",
        ]
        volumes  = [
          "/var/nomad/volumes/pi-hole/etc-pihole/:/etc/pihole/",
          "/var/nomad/volumes/pi-hole/etc-dnsmasq.d/:/etc/dnsmasq.d/",
          "/var/nomad/volumes/pi-hole/var-log/pihole.log:/var/log/pihole.log",
        ]
        cap_add = ["net_admin", "setfcap"]
      }
      resources {
        cpu    = 100
        memory = 200
      }
    }
  }
}
