# Nomad Clien Configuration

datacenter = "raspi-local"
region     = "local"
name       = "nomad-dev-raspi-srv2"
data_dir   = "/opt/nomad"

client {
  enabled = true
  servers = ["<tailscale-nomad-client-ip>"]
  server_join {
    retry_join = ["<tailscale-nomad-client-ip>"]
  }
  host_network "default" {
    interface      = "tailscale-private"
#    reserved_ports = "80,443"
  }
  #host_network "wg-private" {
  #  interface      = "wg-hashi"
  #  reserved_ports = "80,443"
  #}
  host_network "tailscale-private" {
    interface  = "tailscale0"
  }
  host_network "public" {
    interface = "enp0s3"
#    reserved_ports = "80,443"
  }
}

addresses {
  http = "<tailscale-nomad-clienet-ip>"
  rpc  = "<tailscale-nomad-clienet-ip>"
  serf = "<tailscale-nomad-clienet-ip>"
}

advertise {
  http = "<tailscale-nomad-clienet-ip>"
  rpc  = "<tailscale-nomad-clienet-ip>"
  serf = "<tailscale-nomad-clienet-ip>"
}

tls {
  http      = true
  rpc       = true
  ca_file   = "/etc/nomad.d/certs/nomad/nomad-ca.pem"
  cert_file = "/etc/nomad.d/certs/nomad/nomad-cert.pem"
  key_file  = "/etc/nomad.d/certs/nomad/nomad-key.pem"
  verify_server_hostname = true
  verify_https_client    = false
}

consul {
  address               = "<tailscale-consul-client-ip>:8501"
  client_service_name   = "nomad-client"
  token                 = "<consul-token>"
  ssl                   = true
  ca_file               = "/etc/nomad.d/certs/consul/consul-ca.pem"
  cert_file             = "/etc/nomad.d/certs/consul/consul-cert.pem"
  key_file              = "/etc/nomad.d/certs/consul/consul-key.pem"
}

vault {
  enabled               = true
  address               = "https://<tailscale-vault-hostname>:8200"
  tls_server_name       = "client.vault.hashistack.hamsterwheel.xyz"
  ca_file               = "/etc/vault.d/certs/vault/Hashistack_Vault_CA_Root.pem"
  cert_file             = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem"
  key_file              = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.key"
}

acl {
  enabled = true
}

plugin "docker" {
  config {
    volumes {
      enabled = true
    }

    allow_caps = ["audit_write", "chown", "dac_override", "fowner", "fsetid", "kill", "mknod","net_bind_service", "setfcap", "setgid", "setpcap", "setuid", "sys_chroot", "NET_RAW", "net_admin"]
  }
}