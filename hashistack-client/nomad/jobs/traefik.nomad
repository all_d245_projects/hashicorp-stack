job "traefik" {
  region      = "local"
  datacenters = ["raspi-local"]
  type        = "service"

  group "traefik-lb" {
    count = 1

    restart {
      attempts = 10
      interval = "5m"
      delay    = "25s"
      mode     = "delay"
    }

    network {
      mode = "bridge"

      port "public-http" {
        static = 80
        host_network = "public"
      }

      port "public-https" {
        static = 443
        host_network = "public"
      }

      port "private-http" {
        static = 80
        host_network = "tailscale-private"
      }

      port "private-https" {
        static = 443  
        host_network = "tailscale-private"
      }

      port "ping" {
        to = 8082
        host_network = "tailscale-private"
      }
    }

    service {
      name = "traefik"
      
      tags = [
        "traefik.enable=true",
        "traefik.http.routers.traefik.entrypoints=https",
        "traefik.http.routers.traefik.rule=Host(`traefik.hashistack.hamsterwheel.xyz`)",
        "traefik.http.routers.traefik.tls=true",
        "traefik.http.routers.traefik.tls.certresolver=le-prod",
        "traefik.http.routers.traefik.service=api@internal",
        "traefik.http.routers.traefik.middlewares=redirect-to-https@file"
      ]
 
      port = "ping"     
      connect {
        native = true
      }

      check {
        name     = "traefik healthcheck"
        type     = "http"
        path     = "/ping"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "traefik" {
      driver = "docker"
      vault {
        policies = ["traefik-read"]
      }
      template {
        data = <<EOH
         {{ with secret  "kv_hamsterwheel/data/traefik/config/tls/acme" }}
         LINODE_TOKEN="{{ .Data.data.LINODE_API_TOKEN }}"
         {{ end }}
         EOH
        destination = "${NOMAD_SECRETS_DIR}/linode-api-token.env"
        env         = true
      }
      config {
        image = "traefik:v2.5"
        ports = ["private-http","private-https","public-http","public-https"]
        args  = [
          # Entrypoints
          "--entryPoints.http.address=:80",
          "--entryPoints.https.address=:443",
          "--entryPoints.ping.address=:8082",

          "--ping.entryPoint=ping",
          "--api.dashboard=true",
          "--log.level=debug",
          "--ping=true",
          "--ping.entryPoint=ping",

          # Access logs
          "--accesslog=true",
          "--accesslog.filepath=${NOMAD_ALLOC_DIR}/logs/access.log",
          "--accesslog.bufferingsize=100",
          "--accesslog.fields.defaultmode=keep",
          "--accesslog.fields.headers.defaultmode=keep",
          "--accesslog.fields.headers.names.Authorization=drop",

          # Lets Encrypt PROD 
          "--certificatesresolvers.le-prod.acme.email=asaltson@gmail.com",
          "--certificatesresolvers.le-prod.acme.dnschallenge.provider=linode",
          "--certificatesresolvers.le-prod.acme.storage=${NOMAD_ALLOC_DIR}/data/letsencrypt/acme-storage/prod-acme.json",

          # Lets Encrypt DEV
          "--certificatesresolvers.le-dev.acme.email=asaltson@gmail.com",
          "--certificatesresolvers.le-dev.acme.dnschallenge.provider=linode",
          "--certificatesresolvers.le-dev.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory",
          "--certificatesresolvers.le-dev.acme.storage=${NOMAD_ALLOC_DIR}/data/letsencrypt/acme-storage/dev-acme.json",

          # Traefik config file provider
          "--providers.file.directory=${NOMAD_ALLOC_DIR}/data/traefik-configs",
          "--providers.file.watch=true",

          # Consul Catalog provider
          "--providers.consulcatalog.connectaware=true",
          "--providers.consulcatalog.connectbydefault=true",
          "--providers.consulcatalog.exposedByDefault=false",
          "--providers.consulcatalog.servicename=traefik",
          "--providers.consulcatalog.endpoint.scheme=https"
        ]

        # Traefik configs - file provider
        mount {
          type = "bind"
          target = "${NOMAD_ALLOC_DIR}/data/traefik-configs"
          source = "/var/nomad/volumes/traefik/configs"
          readonly = false
        }

        # Letsencrypt ACME Storage
        mount {
          type     = "volume"
          target   = "${NOMAD_ALLOC_DIR}/data/letsencrypt/acme-storage"
          source   = "letsencrypt-storage"
          readonly = false
        }
      }

      resources {
        cpu    = 500
        memory = 200
      }
    }
  }
}
