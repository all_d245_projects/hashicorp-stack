# Consul Client Configuration

primary_datacenter         = "raspi-local"
datacenter                 = "raspi-local"
domain                     = "hashistack.hamsterwheel.xyz"
node_name                  = "consul-dev-raspi-srv2"
data_dir                   = "/opt/consul"
bind_addr                  = "<tailscale-consul-client-ip>"
client_addr                = "<tailscale-consul-client-ip>"
encrypt                    = "<encryption-key>"
disable_remote_exec        = true
enable_local_script_checks = true
retry_join                 = ["<tailscale-consul-server-ip>"]

verify_incoming        = false
verify_incoming_rpc    = true
verify_outgoing        = true
verify_server_hostname = true

ca_file                = "/etc/consul.d/certs/consul/consul-ca.pem"
cert_file              = "/etc/consul.d/certs/consul/consul-cert.pem"
key_file               = "/etc/consul.d/certs/consul/consul-key.pem"

ui_config {
    enabled = false
}

ports {
  grpc  = 8502
  https = 8501
  http  = 8500
}

connect {
  enabled = true
}

acl = {
  enabled = true
  default_policy = "deny"
  enable_token_persistence = true
  tokens {
     default = "<consul-token>"
  }
}