vault {
  address      = "https://<tailscale-vault-hostname>:8200"
  unwrap_token = false
  renew_token  = false

  ssl {
    enabled     = true
    cert        = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem"
    key         = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.key"
    ca_cert     = "/etc/vault.d/certs/vault/Hashistack_Vault_CA_Root.pem"
    server_name = "client.vault.hashistack.hamsterwheel.xyz"
  }
}

## NOMAD TLS-CERT ##
template {
  source      = "/etc/nomad.d/templates/nomad-cert.pem.tpl"
  destination = "/etc/nomad.d/certs/nomad/nomad-cert.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC nomad-cert && chown nomad:nomad /etc/nomad.d/certs/nomad/nomad-cert.pem && date && service nomad reload -http-addr=http://<tailscale-nomad-server-ip>:4646 || true'"
}

## NOMAD TLS-KEY ##
template {
  source      = "/etc/nomad.d/templates/nomad-key.pem.tpl"
  destination = "/etc/nomad.d/certs/nomad/nomad-key.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC nomad-key && chown nomad:nomad /etc/nomad.d/certs/nomad/nomad-key.pem && date && service nomad reload -http-addr=http://<tailscale-nomad-server-ip>:4646 || true'"
}

## NOMAD CA ##
template {
  source      = "/etc/nomad.d/templates/nomad-ca.pem.tpl"
  destination = "/etc/nomad.d/certs/nomad/nomad-ca.pem"
  command     = "bash -c 'echo EXEC nomad-ca && chown nomad:nomad /etc/nomad.d/certs/nomad/nomad-ca.pem && date && service nomad reload -http-addr=http://<tailscale-nomad-server-ip>:4646 || true'"
}


## CONSUL TLS-CERT ##
template {
  source      = "/etc/nomad.d/templates/consul-cert.pem.tpl"
  destination = "/etc/nomad.d/certs/consul/consul-cert.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC nomad-consul-cert && chown nomad:nomad /etc/nomad.d/certs/consul/consul-cert.pem && date && service nomad reload -http-addr=http://<tailscale-nomad-server-ip>:4646 || true'"
}

## CONSUL TLS-KEY ##
template {
  source      = "/etc/nomad.d/templates/consul-key.pem.tpl"
  destination = "/etc/nomad.d/certs/consul/consul-key.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC nomad-consul-key && chown nomad:nomad /etc/nomad.d/certs/consul/consul-key.pem && date && service nomad reload -http-addr=http://<tailscale-nomad-server-ip>:4646 || true'"
}

## CONSUL CA ##
template {
  source      = "/etc/nomad.d/templates/consul-ca.pem.tpl"
  destination = "/etc/nomad.d/certs/consul/consul-ca.pem"
  command     = "bash -c 'echo EXEC nomad-consul-ca && chown nomad:nomad /etc/nomad.d/certs/consul/consul-ca.pem && date && service nomad reload -http-addr=http://<tailscale-nomad-server-ip>:4646'"
}