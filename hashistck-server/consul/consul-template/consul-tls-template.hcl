vault {
  address      = "https://<tailscale-vault-hostname>:8200"
  unwrap_token = false
  renew_token  = false

  ssl {
    enabled     = true
    cert        = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem"
    key         = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.key"
    ca_cert     = "/etc/vault.d/certs/vault/Hashistack_Vault_CA_Root.pem"
    server_name = "client.vault.hashistack.hamsterwheel.xyz"
  }
}

template {
  source      = "/etc/consul.d/templates/consul-cert.pem.tpl"
  destination = "/etc/consul.d/certs/consul/consul-cert.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC consul-cert && chown consul:consul /etc/consul.d/certs/consul/consul-cert.pem && date && CONSUL_HTTP_SSL=false consul reload -http-addr=http://<tailscale-consul-server-hostname>:8500 || true'"
}

template {
  source      = "/etc/consul.d/templates/consul-key.pem.tpl"
  destination = "/etc/consul.d/certs/consul/consul-key.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC consul-key && chown consul:consul /etc/consul.d/certs/consul/consul-key.pem && date && CONSUL_HTTP_SSL=false consul reload -http-addr=http://<tailscale-consul-server-hostname>:8500 || true'"
}

template {
  source      = "/etc/consul.d/templates/consul-ca.pem.tpl"
  destination = "/etc/consul.d/certs/consul/consul-ca.pem"
  command     = "bash -c 'echo EXEC consul-ca && chown consul:consul /etc/consul.d/certs/consul/consul-ca.pem && date && CONSUL_HTTP_SSL=false consul reload -http-addr=http://<tailscale-consul-server-hostname>:8500 || true'"
}