job "portainer" {
  datacenters = ["raspi-local"]
  region = "local"
  type = "service"

  update {
    stagger      = "30s"
    max_parallel = 1
  }

  group "portainer" {
    count = 1

    network {
      mode = "bridge"

      port "ui" {
        static = 9000
        to = 9000
        host_network = "tailscale-private"
      }

      port "agent" {
        to = 9001
        host_network = "tailscale-private"
      }
    }

    service {
      name = "portainer"
      
      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=false",
        "traefik.http.routers.portainer.entrypoints=https",
        "traefik.http.routers.portainer.rule=Host(`portainer.hamsterwheel.xyz`)",
        "traefik.http.routers.portainer.tls=true",
        "traefik.http.routers.portainer.tls.certresolver=le-prod",
        "traefik.http.services.portainer.loadbalancer.server.port=9000",
        "traefik.http.routers.portainer.middlewares=redirect-to-https@file"
      ]
 
      port = "ui"     
      #check {
      #  name     = "pi-hole healthcheck"
      #  type     = "http"
      #  path     = "/"
      #  interval = "10s"
      #  timeout  = "2s"
      #}
    }

    task "portainer-agent" {
      driver = "docker"

      env {
        AGENT_PORT = "${NOMAD_PORT_agent}"
      }

      config {
        image = "portainer/agent:2.11.0"

        # Bind mount for docker socket
        mount {
          type = "bind"
          target = "/var/run/docker.sock"
          source = "/var/run/docker.sock"
          readonly = true
        }
      }
    }

    task "portainer" {
      driver = "docker"

      config {
        image = "portainer/portainer-ce:2.11.0"
        ports = ["ui"]
        args = [
          "-H", "tcp://${NOMAD_ADDR_agent}",
          "--tlsskipverify"
        ]

        # Volume mount for portainer-data
        mount {
          type = "volume"
          target = "/data"
          source = "portainer-data"
        }
      }
    }
  }
}
