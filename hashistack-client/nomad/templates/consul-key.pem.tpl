{{ with secret "pki_int_hamsterwheel/issue/consul-hashistack-tls" "common_name=client.raspi-local.hashistack.hamsterwheel.xyz" "alt_names=localhost" "ip_sans=127.0.0.1,<tailscale-nomad-client-ip>" }}
{{ .Data.private_key }}
{{ end }}
