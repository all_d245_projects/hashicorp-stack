job "gitlab-runner" {
  datacenters = ["raspi-local"]
  region = "local"
  type = "service"

  update {
    stagger      = "30s"
    max_parallel = 1
  }

  group "gitlab-runner" {
    count = 1

    network {
      mode = "bridge"
    }

    task "gitlab-runner" {
      driver = "docker"
      vault {
        policies = ["gitlab-runner-read"]
      }
      template {
        data = <<EOH
         {{ with secret  "kv_hamsterwheel/data/gitlab-runner/config" }}
         GITLAB_RUNNERE_HOST="{{ .Data.data.GITLAB_RUNNER_HOST }}"
         GITLAB_RUNNER_REGISTRATION_TOKEN="{{ .Data.data.GITLAB_RUNNER_REGISTRATION_TOKEN }}"
         {{ end }}
         EOH
        destination = "${NOMAD_SECRETS_DIR}/gitlab-runner-config.env"
        env         = true
      }

      service {
        name = "gitlab-runner"
        address_mode = "driver"
        port = 9252
      }

      logs {
        max_files = 10
        max_file_size = 10
      }

      config {
        image = "gitlab/gitlab-runner:latest"
        mount {
          type = "bind"
          target = "/etc/gitlab-runner"
          source = "/var/nomad/volumes/gitlab-runner"
          readonly = false
        }

        # Bind mount for docker socket
        mount {
          type = "bind"
          target = "/var/run/docker.sock"
          source = "/var/run/docker.sock"
          readonly = true
        }
      }

      resources {
         cpu        = 500
         memory     = 500
       }
    }
  }
}
