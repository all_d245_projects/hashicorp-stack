# Vault Server Configuration
ui            = true
disable_mlock = true

api_addr     = "https://<tailscale-vault-ip>:8200"
cluster_addr = "https://<tailscale-vault-ip>:8201"

# loopback interface
listener "tcp" {
  address            = "127.0.0.1:8200"
  tls_client_ca_file = "/etc/vault.d/certs/vault/Hashistack_Vault_CA_Root.pem"
  tls_cert_file      = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem"
  tls_key_file       = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.key"
}

# private tailscale interface
listener "tcp" {
  address            = "<tailscale-vault-hostname>:8200"
  tls_client_ca_file = "/etc/vault.d/certs/vault/Hashistack_Vault_CA_Root.pem"
  tls_cert_file      = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem"
  tls_key_file       = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.key"
}

service_registration "consul" {
  scheme        = "https"
  address       = "<tailscale-consul-host>:8501"
  service       = "vault"
  token         = "<consul-token>"
  tls_ca_file   = "/etc/vault.d/certs/consul/consul-ca.pem"
  tls_cert_file = "/etc/vault.d/certs/consul/consul-cert.pem"
  tls_key_file  = "/etc/vault.d/certs/consul/consul-key.pem"
  service_tags  = "traefik.enable=true,traefik.consulcatalog.connect=false,traefik.tcp.routers.vault.entrypoints=https,traefik.tcp.routers.vault.tls.passthrough=true,traefik.tcp.routers.vault.rule=HostSNI(`vault.hashistack.hamsterwheel.xyz`),traefik.tcp.services.vault.loadbalancer.server.port=8200,traefik.http.routers.vault.middlewares=redirect-to-https@file"
}

#storage "file" {
#  path = "/var/vault/data"
#}

storage "raft" {
  path    = "/opt/vault/raft/"
  node_id = "vault-raspi-srv1"

  retry_join {
    leader_api_addr = "https://<tailscale-vault-host>:8200"
  }
}