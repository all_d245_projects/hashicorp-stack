{{ with secret "pki_int_hamsterwheel/issue/nomad-hashistack-tls" "common_name=server.local.nomad" "alt_names=localhost,nomad.hashistack.hamsterwheel.xyz,dev-raspi-srv1-hashistack-server" "ip_sans=127.0.0.1,<tailscale-nomad-server-ip>" }}
{{ .Data.private_key }}
{{ end }}
