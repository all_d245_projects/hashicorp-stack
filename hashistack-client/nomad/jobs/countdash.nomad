job "countdash" {
  datacenters = ["raspi-local"]
  region = "local"

  group "api" {
    network {
      mode = "bridge"
      port "connect-proxy-count-api" {
        to = -1
        host_network = "tailscale-private"
      }
    }

    service {
      name = "count-api"
      port = "9001"

      connect {
        sidecar_service {}
      }
    }

    task "web" {
      driver = "docker"

      config {
        image = "hashicorpnomad/counter-api:v3"
      }
      resources {
         cpu        = 50
         memory     = 50
       }
    }
  }

  group "dashboard" {
    network {
      mode = "bridge"

      port "http" {
        static = 9002
        to     = 9002
        host_network = "tailscale-private"
      }
      port "connect-proxy-count-dashboard" {
        to = -1
        host_network = "tailscale-private"
      }
    }

    service {
      name = "count-dashboard"
      port = "http"
      tags = [
        "traefik.enable=true",
        "traefik.consulcatalog.connect=false",
        "traefik.http.routers.countdash.entrypoints=https",
        "traefik.http.routers.countdash.rule=Host(`countdash.hamsterwheel.xyz`)",
        "traefik.http.routers.countdash.tls=true",
        "traefik.http.routers.countdash.tls.certresolver=le-prod",
        "traefik.http.services.countdash.loadbalancer.server.port=9002",
        "traefik.http.routers.countdash.middlewares=redirect-to-https@file"
      ]
      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "count-api"
              local_bind_port  = 8080
            }
          }
        }
      }
    }

    task "dashboard" {
      driver = "docker"

      env {
        COUNTING_SERVICE_URL = "http://${NOMAD_UPSTREAM_ADDR_count_api}"
      }

      config {
        image = "hashicorpnomad/counter-dashboard:v3"
      }
      resources {
         cpu        = 50
         memory     = 50
       }
    }
  }
}

