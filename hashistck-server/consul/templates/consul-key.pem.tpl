{{ with secret "pki_int_hamsterwheel/issue/consul-hashistack-tls" "common_name=server.raspi-local.hashistack.hamsterwheel.xyz" "alt_names=localhost,consul.hashistack.hamsterwheel.xyz,dev-raspi-srv1-hashistack-server" "ip_sans=127.0.0.1,<tailscale-consul-server-ip>" }}
{{ .Data.private_key }}
{{ end }}
