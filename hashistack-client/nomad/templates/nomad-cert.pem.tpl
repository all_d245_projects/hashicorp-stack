{{ with secret "pki_int_hamsterwheel/issue/nomad-hashistack-tls" "common_name=client.local.nomad" "alt_names=localhost" "ip_sans=127.0.0.1,<tailscale-nomad-client-ip>" }}
{{ .Data.certificate }}
{{ end }}
