# Nomad Server Configuration

datacenter = "raspi-local"
region     = "local"
name       =  "nomad-dev-raspi-srv1"
data_dir   = "/opt/nomad"


server {
  enabled = true
  bootstrap_expect = 1
  encrypt    = "<encryption-key>"
}

addresses {
  http = "<tailscale-nomad-server-IP>"
  rpc  = "<tailscale-nomad-server-IP>"
  serf = "<tailscale-nomad-server-IP>"
}

advertise {
  http = "<tailscale-nomad-server-IP>"
  rpc  = "<tailscale-nomad-server-IP>"
  serf = "<tailscale-nomad-server-IP>"
}

tls {
  http                   = true
  rpc                    = true
  ca_file                = "/etc/nomad.d/certs/nomad/nomad-ca.pem"
  cert_file              = "/etc/nomad.d/certs/nomad/nomad-cert.pem"
  key_file               = "/etc/nomad.d/certs/nomad/nomad-key.pem"
  verify_server_hostname = true
  verify_https_client    = false
}


consul {
  address               = "<tailscale-consul-server-ip>:8501"
  server_service_name   = "nomad-server"
  tags                  = [
    "raspi-srv1",
    "raspi-local",
    "server",
    "nomad",
    "wg-private",
    "traefik.enable=true",
    "traefik.consulcatalog.connect=false",
    "traefik.tcp.routers.nomad-server.entrypoints=https",
    "traefik.tcp.routers.nomad-server.tls.passthrough=true",
    "traefik.tcp.routers.nomad-server.rule=HostSNI(`nomad.hashistack.hamsterwheel.xyz`)",
    "traefik.tcp.services.nomad-server.loadbalancer.server.port=4646",
    "traefik.http.routers.nomad-server.middlewares=redirect-to-https@file"
  ]
  token                 = "<consul-token>"
  ssl                   = true
  ca_file               = "/etc/nomad.d/certs/consul/consul-ca.pem"
  cert_file             = "/etc/nomad.d/certs/consul/consul-cert.pem"
  key_file              = "/etc/nomad.d/certs/consul/consul-key.pem"  
}

vault {
  enabled               = true
  tls_server_name       = "client.vault.hashistack.hamsterwheel.xyz"
  ca_file               = "/etc/vault.d/certs/vault/Hashistack_Vault_CA_Root.pem"
  cert_file             = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle.pem"
  key_file              = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.key"
  address               = "https://<tailscale-vault-hostname>:8200"
  create_from_role      = "nomad-cluster"
}

acl {
  enabled = true
}