# Consul Server

primary_datacenter         = "raspi-local"
datacenter                 = "raspi-local"
node_name                  = "consul-dev-raspi-srv1"
domain                     = "hashistack.hamsterwheel.xyz"
data_dir                   = "/opt/consul"
bind_addr                  = "<tailscale-consul-server-ip>"
client_addr                = "<tailscale-consul-server-ip>"
server                     = true
bootstrap_expect           = 1
encrypt                    = "<encryption-key>"
disable_remote_exec        = true
enable_local_script_checks = true

verify_incoming        = false
verify_incoming_rpc    = true
verify_outgoing        = true
verify_server_hostname = true

ca_file                = "/etc/consul.d/certs/consul/consul-ca.pem"
cert_file              = "/etc/consul.d/certs/consul/consul-cert.pem"
key_file               = "/etc/consul.d/certs/consul/consul-key.pem"


ui_config {
    enabled = true
}


ports {
  grpc  = 8502
  https = 8501
  http  = 8500
}


connect {
  enabled = true
}


acl = {
  enabled = true
  default_policy = "deny"
  enable_token_persistence = true
  tokens {
     default = "<consul-token>"
  }
}