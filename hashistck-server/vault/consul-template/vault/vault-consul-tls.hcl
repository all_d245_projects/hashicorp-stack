vault {
  address      = "https://127.0.0.1:8200"
  unwrap_token = false
  renew_token  = false

  ssl {
    enabled     = true
    cert        = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.bundle..pem"
    key         = "/etc/vault.d/certs/vault/vault.hashistack.hamsterwheel.xyz.key"
    server_name = "server.vault.hashistack.hamsterwheel.xyz"
  }
}

template {
  source      = "/etc/vault.d/templates/consul-cert.pem.tpl"
  destination = "/etc/vault.d/certs/consul/consul-cert.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC VAULT consul-cert && chown vault:vault /etc/vault.d/certs/consul/consul-cert.pem && date && service vault reload'"
}

template {
  source      = "/etc/vault.d/templates/consul-key.pem.tpl"
  destination = "/etc/vault.d/certs/consul/consul-key.pem"
  perms       = 0700
  command     = "bash -c 'echo EXEC VAULT consul-key && chown vault:vault /etc/vault.d/certs/consul/consul-key.pem && date && service vault reload'"
}

template {
  source      = "/etc/vault.d/templates/consul-ca.pem.tpl"
  destination = "/etc/vault.d/certs/consul/consul-ca.pem"
  command     = "bash -c 'echo EXEC VAULT consul-ca && chown vault:vault /etc/vault.d/certs/consul/consul-ca.pem && date && service vault reload'"
}