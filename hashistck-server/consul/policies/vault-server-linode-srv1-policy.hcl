# Vault Server Linode srv1 Policy

key_prefix "vault/" {
  policy = "write"
}

service "vault-server-linode-srv1" {
  policy = "write"
}

agent_prefix "" {
	policy = "read"
}

session_prefix "" {
	policy = "write"
}
