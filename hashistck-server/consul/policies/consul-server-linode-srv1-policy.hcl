# Consul Server Linode srv1 Policy

node "consul-server-linode-srv1" {
  policy = "write"
}

service_prefix "" {
  policy = "write"
}

agent_prefix "" {
	policy = "write"
}
